// helloWorld.js

// Define a function to print "Hello, World!"
function sayHello() {
  console.log("Hello, World!");
}

// Call the function to print the message
sayHello();